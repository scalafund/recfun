package recfun

object RecFun extends RecFunInterface:

  def main(args: Array[String]): Unit =
    println("Pascal's Triangle")
    for row <- 0 to 10 do
      for col <- 0 to row do
        print(s"${pascal(col, row)} ")
      println()

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if c == 0 || r == 0 || c == r then 1 else pascal(c - 1 , r - 1 ) + pascal(c , r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    var open = 0
    var close = 0
    var flag = false
    balance2(chars, open, close, flag)

  }
    def balance2(chars: List[Char], open: Int, close: Int, flag: Boolean): Boolean = {
      if chars.isEmpty && open == close && !flag then true
      else if chars.isEmpty && (open != close || flag) then false
      else if chars.head == '(' then balance2(chars.tail, open+1, close, true)
      else if chars.head == ')' then balance2(chars.tail, open, close+1, false)
      else balance2(chars.tail, open, close, flag)

  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    if money == 0 then 1
    else if money < 0 then 0
    else if money >= 0 && coins.isEmpty then 0
    else countChange(money, coins.tail) + countChange((money-coins.head) , coins)
    
    
  }
